class RollingField:
    def __init__(self, x, y):
        self.row_pointer = 0
        self.grid = [[0 for _ in range(x)] for _ in range(y)]

    def _y_adj(self, y):
        return (y + self.row_pointer) % self.get_height()

    def set(self, y, x, value):
        self.grid[self._y_adj(y)][x] = value

    def get(self, y, x):
        return self.grid[self._y_adj(y)][x]

    def set_line(self, y, line):
        self.grid[self._y_adj(y)] = line

    def get_line(self, y):
        return self.grid[self._y_adj(y)]

    def roll_up(self):
        self.row_pointer += 1

    def get_height(self):
        return len(self.grid)

    def get_width(self):
        return len(self.grid[0])
