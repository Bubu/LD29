#!/usr/bin/env python

from config import *

import pg_interface as _if
import TerrainGenerator
import root

from rollingfield import RollingField


class game:
    def __init__(self):
        self.energy = ENERGY_MAX
        self.depthscore = RES_Y // 50
        self._if = _if.interface(self)
        self.reset()

    def setup(self):
        self.decals.append(decal(PLANT, 0, (RES_X // 100) * 50 - 18, 150, 100))
        self.decals.append(decal(CLOUD1, 5, 15, 90, 199))
        self.decals.append(decal(CLOUD2, 7, 870, 87, 201))
        self.decals.append(decal(CLOUD3, 4, 400, 58, 104))
        self.decals.append(decal(CLOUD4, 10, 1200, 50, 82))
        self.roots.append(root.root(self, (3, RES_X // 100), DOWN))

        self._if.score()
        self._if.handlemusic()

    def initial_terrain(self):
        field = RollingField(RES_X // 50, RES_Y // 50)
        for i in range(3):
            field.set_line(i, [AIR] * field.get_width())
        for i in range(3, RES_Y // 50):
            field.set_line(i, self.generator.getLine())
            self._if.genground(self.level)
            self.level += 1
        field.set(3, RES_X // 100, SUP)
        return field

    def run(self):
        self._if.run()

    def close(self):
        self._if.close()

    def scroll_up(self):
        self.grid.roll_up()
        remove = []
        for r in self.roots:
            r.pos = (r.pos[0] - 1, r.pos[1])
            if r.pos[0] < 0:
                remove.append(r)
        for rem in remove:
            self.roots.remove(rem)
        remove = []
        for d in self.decals:
            d.scrollUp()
            if d.isDead():
                remove.append(d)
        for rem in remove:
            self.decals.remove(rem)
        self.grid_update()

    def grid_update(self):
        self.grid.set_line(self.grid.get_height() - 1, self.generator.getLine())
        self._if.genground(self.level)
        self.level += 1

    def move(self, check_dir):
        self.energy -= 1
        self.lowest_ypos = 0
        stuck_counter = 0
        for root in self.roots:
            oldpos = root.pos
            olddir = root.dir
            if check_dir == DOWN:
                dir = root.check_down()
            elif check_dir == RIGHT:
                dir = root.check_right()
            elif check_dir == LEFT:
                dir = root.check_left()
            if dir is not None:
                self.updateSingleTile(root, dir, olddir, oldpos)
                self._if.score()
            if oldpos == root.pos:
                stuck_counter += 1
        if stuck_counter == len(self.roots):
            self.init_game_over()
        self.checkScroll()

    def triggerSplit(self):
        self.newroots = []
        self.lowest_ypos = 0
        self.energy -= 10
        for r in self.roots:
            dir1 = None
            dir2 = None
            oldpos = r.pos
            olddir = r.dir
            dir1, dir2 = r.check_split()
            if dir1 is not None:
                if dir2 is not None:
                    newpos1, newpos2 = self.getnewpos(oldpos, dir1, dir2)
                    self.doAction(self.grid.get(*newpos1), r)
                    self.doAction(self.grid.get(*newpos2), r)
                    self.lowest_ypos = max(self.lowest_ypos, newpos1[0], newpos2[0])
                    if olddir == UP:
                        if dir1 == RIGHT:
                            if dir2 == LEFT:
                                newtile = DOWNRIGHTLEFT
                                newtile1 = SLEFT
                                newtile2 = SRIGHT
                            elif dir2 == UP:
                                newtile = DOWNRIGHTUP
                                newtile1 = SLEFT
                                newtile2 = SDOWN
                        elif dir1 == UP:
                            if dir2 == LEFT:
                                newtile = DOWNUPLEFT
                                newtile1 = SDOWN
                                newtile2 = SRIGHT
                    elif olddir == DOWN:
                        if dir1 == LEFT:
                            if dir2 == RIGHT:
                                newtile = UPLEFTRIGHT
                                newtile1 = SRIGHT
                                newtile2 = SLEFT
                            elif dir2 == DOWN:
                                newtile = UPLEFTDOWN
                                newtile1 = SRIGHT
                                newtile2 = SUP
                        elif dir1 == DOWN:
                            if dir2 == RIGHT:
                                newtile = UPDOWNRIGHT
                                newtile1 = SUP
                                newtile2 = SLEFT
                    elif olddir == RIGHT:
                        if dir1 == DOWN:
                            if dir2 == UP:
                                newtile = LEFTDOWNUP
                                newtile1 = SUP
                                newtile2 = SDOWN
                            elif dir2 == RIGHT:
                                newtile = LEFTDOWNRIGHT
                                newtile1 = SUP
                                newtile2 = SLEFT
                        elif dir1 == RIGHT:
                            if dir2 == UP:
                                newtile = LEFTRIGHTUP
                                newtile1 = SLEFT
                                newtile2 = SDOWN
                    elif olddir == LEFT:
                        if dir1 == UP:
                            if dir2 == DOWN:
                                newtile = RIGHTUPDOWN
                                newtile1 = SDOWN
                                newtile2 = SUP
                            elif dir2 == LEFT:
                                newtile = RIGHTUPLEFT
                                newtile1 = SDOWN
                                newtile2 = SRIGHT
                        elif dir1 == LEFT:
                            if dir2 == DOWN:
                                newtile = RIGHTLEFTDOWN
                                newtile1 = SRIGHT
                                newtile2 = SUP
                    self.grid.set(*oldpos, newtile)
                    self.grid.set(*newpos1, newtile1)
                    self.grid.set(*newpos2, newtile2)
                    self._if.update([oldpos, newpos1, newpos2])
                    self._if.score()
                else:
                    self.updateSingleTile(r, dir1, olddir, oldpos)
                    self._if.score()
        self.roots += self.newroots
        self.checkScroll()

    def updateSingleTile(self, root, dir, olddir, oldpos):
        newpos = root.pos
        if newpos[0] < 0 or newpos[1] < 0:
            raise ValueError("<0: Old: ", olddir, " New: ", dir)
        self.doAction(self.grid.get(*newpos), root)
        if newpos[0] > self.lowest_ypos:
            self.lowest_ypos = newpos[0]
        if dir == DOWN:
            self.grid.set(*newpos, SUP)
            if olddir == DOWN:
                self.grid.set(*oldpos, UPDOWN)
            elif olddir == RIGHT:
                self.grid.set(*oldpos, LEFTDOWN)
            elif olddir == LEFT:
                self.grid.set(*oldpos, RIGHTDOWN)
        elif dir == UP:
            self.grid.set(*newpos, SDOWN)
            if olddir == UP:
                self.grid.set(*oldpos, DOWNUP)
            elif olddir == LEFT:
                self.grid.set(*oldpos, RIGHTUP)
            elif olddir == RIGHT:
                self.grid.set(*oldpos, LEFTUP)
        elif dir == RIGHT:
            self.grid.set(*newpos, SLEFT)
            if olddir == UP:
                self.grid.set(*oldpos, DOWNRIGHT)
            elif olddir == DOWN:
                self.grid.set(*oldpos, UPRIGHT)
            elif olddir == RIGHT:
                self.grid.set(*oldpos, LEFTRIGHT)
        elif dir == LEFT:
            self.grid.set(*newpos, SRIGHT)
            if olddir == UP:
                self.grid.set(*oldpos, DOWNLEFT)
            elif olddir == LEFT:
                self.grid.set(*oldpos, RIGHTLEFT)
            elif olddir == DOWN:
                self.grid.set(*oldpos, UPLEFT)
        self._if.update([oldpos, newpos])

    def checkScroll(self):
        if self.lowest_ypos >= (RES_Y // 50 - 5):
            self.scroll_up()
            self._if.redrawGrid()

    def getnewpos(self, oldpos, dir1, dir2):
        d = {DOWN: (1, 0), UP: (-1, 0), LEFT: (0, -1), RIGHT: (0, 1)}
        newpos1 = oldpos[0] + d[dir1][0], oldpos[1] + d[dir1][1]
        newpos2 = oldpos[0] + d[dir2][0], oldpos[1] + d[dir2][1]
        if newpos1[0] < 0 or newpos1[1] < 0:
            raise ValueError("<0: New: ", dir1, dir2, newpos1)
        if newpos2[0] < 0 or newpos2[1] < 0:
            raise ValueError("<0: New: ", dir1, dir2, newpos2)
        return newpos1, newpos2

    def reset(self):
        self.generator = TerrainGenerator.TerrainGenerator()
        self.level = 3
        self.game_over = False
        self.grid = self.initial_terrain()
        self.decals = []
        self.roots = []
        self.setup()
        self._if.stop_sounds()
        self._if.redrawGrid()

    def check_energy(self):
        if self.energy <= 0:
            self.init_game_over()

    def init_game_over(self):
        if self.game_over == False:
            self._if.stopMusic()
            self._if.play_sound(GAME_OVER)
        self.game_over = True
        if self.level > self.depthscore:
            self.depthscore = self.level
        self._if.refresh_depthscore()
        self._if.game_over()

    def doAction(self, type, root):
        if type == WATER:
            self.energy += VAL_WATER
            self._if.play_sound(S_WATER)
        if type == URANIUM:
            self.energy -= VAL_URANIUM
            self._if.play_sound(S_URANIUM)
        if type == MINERAL:
            root.setStoneCrusher()
            self._if.play_sound(S_MINERAL1)
        if type == MINERAL2:
            self.energy += VAL_MINERAL2
            self._if.play_sound(S_MINERAL2)
        if type == MINERAL3:
            for r in self.roots:
                r.setStoneCrusher_4_all_roots()
            self._if.play_sound(S_MINERAL3)
        if type in STONES:
            root.eatStone()
            self._if.play_sound(S_STONE_CRUSH)

    def getCrusherCoords(self):
        return [r.pos for r in self.roots if r.stoner_rock > 0]


class decal:
    def __init__(self, type, y, x, sizey, sizex):
        self.type = type
        self.y = y
        self.x = x
        self.sizey = sizey
        self.sizex = sizex

    def scrollUp(self):
        self.y -= 50

    def isDead(self):
        return self.y + self.sizey <= 0


myGame = game()
myGame.run()
myGame.close()
